/*******************************************************************************
 * Copyright (c) 2013 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.shared.ui.test.core;

import java.lang.reflect.Method;

import junit.framework.TestCase;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.xtend.shared.ui.core.internal.JDTUtil;
import org.eclipse.xtend.shared.ui.core.internal.ResourceID;

/**
 * Test for class {@link org.eclipse.xtend.shared.ui.core.internal.JDTUtil}.
 * @author thoms - Initial contribution and API
 */
public class JDTUtilTest extends TestCase {
	public void test_path () throws Exception {
		Method method = JDTUtil.class.getDeclaredMethod("path", ResourceID.class);
		try {
			method.setAccessible(true);
			assertEquals (new Path("foo/foo2/Bar.ext"), method.invoke(null, new ResourceID("foo::foo2::Bar", "ext")));
		} finally {
			method.setAccessible(false);
		}
	}
	public void test_toResourceID () throws Exception {
		Method method = JDTUtil.class.getDeclaredMethod("toResourceID", IPath.class);
		try {
			method.setAccessible(true);
			assertEquals (new ResourceID("foo::foo2::Bar", "ext"), method.invoke(null, new Path("foo/foo2/Bar.ext")));
		} finally {
			method.setAccessible(false);
		}
	}
}
