/*******************************************************************************
 * Copyright (c) 2013 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.shared.ui.test.core;

import org.eclipse.xtend.shared.ui.core.internal.ResourceID;

import junit.framework.TestCase;

/**
 * @author thoms - Initial contribution and API
 */
public class ResourceIDTest extends TestCase {
	public void test_toString () {
		assertEquals("foo.ext", new ResourceID("foo", "ext").toString());
	}
	
	public void test_toFileName () {
		assertEquals("foo/bar.ext", new ResourceID("foo::bar", "ext").toFileName());
	}
	
	public void test_equals () {
		assertEquals(new ResourceID("foo::bar", "ext"), new ResourceID("foo::bar", "ext"));
		assertNotSame(new ResourceID("foo::bar", "xpt"), new ResourceID("foo::bar", "ext"));
		assertNotSame(new ResourceID("foo::bar", "xpt"), new ResourceID("foo::bar", "ext"));
		assertNotSame(new ResourceID("foo::bar", "xpt"), new ResourceID("foo", "xpt"));
	}
}
