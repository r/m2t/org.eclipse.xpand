/*******************************************************************************
 * Copyright (c) 2013 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.typesystem.baseimpl;

import java.util.ArrayList;
import java.util.HashSet;

import junit.framework.TestCase;

import org.eclipse.internal.xtend.type.baseimpl.BuiltinMetaModel;
import org.eclipse.xtend.expression.TypeSystemImpl;

/**
 * Unit test for {@link BuiltinMetaModel}
 * 
 * @author Karsten Thoms - Initial contribution and API
 */
public class BuiltinMetaModelTest extends TestCase {
	private BuiltinMetaModel mm;

	@Override
	protected void setUp() throws Exception {
		mm = new BuiltinMetaModel(new TypeSystemImpl());
	}

	public void test_getTypeForName() {
		assertEquals(mm.getObjectType(), mm.getTypeForName("Object"));
		assertEquals(mm.getVoidType(), mm.getTypeForName("Void"));

		assertEquals(mm.getBooleanType(), mm.getTypeForName("boolean"));
		assertEquals(mm.getBooleanType(), mm.getTypeForName("Boolean"));

		assertEquals(mm.getRealType(), mm.getTypeForName("float")); // Bug#414545
		assertEquals(mm.getRealType(), mm.getTypeForName("double"));
	}

	public void test_getNamespaces() {
		assertTrue(mm.getNamespaces().isEmpty());
	}

	public void test_getKnownTypes() {
		assertEquals(14, mm.getKnownTypes().size());
	}

	public void test_getType() {
		assertEquals(mm.getVoidType(), mm.getType(null));
		assertEquals(mm.getStringType(), mm.getType("foo"));
		assertEquals(mm.getBooleanType(), mm.getType(false));
		assertEquals(mm.getListType(mm.getObjectType()), mm.getType(new ArrayList<Object>()));
		assertEquals(mm.getSetType(mm.getObjectType()), mm.getType(new HashSet<Object>()));
		assertEquals(mm.getTypeType(), mm.getType(mm.getObjectType()));
	}
}
