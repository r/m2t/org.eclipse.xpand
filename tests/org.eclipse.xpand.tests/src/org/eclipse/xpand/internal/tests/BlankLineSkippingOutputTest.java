/*******************************************************************************
 * Copyright (c) 2013 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xpand.internal.tests;

import junit.framework.TestCase;

import org.eclipse.xpand2.output.BlankLineSkippingOutput;
import org.eclipse.xpand2.output.Outlet;

/**
 * @author thoms - Initial contribution and API
 */
public class BlankLineSkippingOutputTest extends TestCase {
	private BlankLineSkippingOutput output;

	@Override
	public void setUp () {
		output = new BlankLineSkippingOutput();
		Outlet outlet = new Outlet("foo");
		outlet.setName("default");
		output.addOutlet(outlet);
		output.openFile("foof", "default");
	}
	public void test_write () {
		output.write("Bar");
	}
}
