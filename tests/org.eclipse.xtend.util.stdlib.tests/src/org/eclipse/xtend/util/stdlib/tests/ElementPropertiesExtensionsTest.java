/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.util.stdlib.tests;

import java.lang.reflect.Field;
import java.util.Map;

import junit.framework.TestCase;

import org.eclipse.xtend.expression.ExecutionContextImpl;
import org.eclipse.xtend.util.stdlib.ElementPropertiesExtensions;

/**
 * @author thoms - Initial contribution and API
 */
public class ElementPropertiesExtensionsTest extends TestCase {
	private ExecutionContextImpl execCtx;
	private ElementPropertiesExtensions ext;


	@Override
	public void setUp () {
		execCtx = new ExecutionContextImpl();
		ext = new ElementPropertiesExtensions();
		ext.setExecutionContext(execCtx);
	}

	public void testPropertyAccess () {
		String element = "TEST";
		// element not accessed yet, thus property is null
		assertNull(ext.getProperty(element, "foo"));
		// set a property value
		ext.setProperty(element, "foo", "bar");
		// now the property must be set
		assertEquals("bar", ext.getProperty(element, "foo"));
		// clear the property
		ext.setProperty(element, "foo", null);
		assertNull(ext.getProperty(element, "foo"));
	}
	
	public void testLegacyMode () throws Exception {
		String element = "TEST";

		Field outerMapField = ElementPropertiesExtensions.class.getDeclaredField("outerMap");
		outerMapField.setAccessible(true);
		assertNull("in default mode 'outerMap' is not set", outerMapField.get(null));
		ElementPropertiesExtensions.setLegacyMode(true);
		assertNotNull("in legacy mode 'outerMap' is set", outerMapField.get(null));
		
		Map<String,Object> outerMap = (Map<String, Object>) outerMapField.get(null);
		assertTrue("No inner map present yet", outerMap.isEmpty());
		
		// set a property value
		ext.setProperty(element, "foo", "bar");
		// now the property must be set
		assertEquals("bar", ext.getProperty(element, "foo"));
		assertNotNull("Check that inner map is present", outerMap.get(element));
		
		// unset legacy mode
		ElementPropertiesExtensions.setLegacyMode(false);
		assertNull("in default mode 'outerMap' is not set", outerMapField.get(null));
	}

}
