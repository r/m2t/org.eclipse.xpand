/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.util.stdlib.tests;

import java.util.Map;

import junit.framework.TestCase;

import org.eclipse.xtend.expression.ExecutionContextImpl;
import org.eclipse.xtend.util.stdlib.AbstractStatefulExtensions;

/**
 * @author thoms - Initial contribution and API
 */
public class StatefulExtensionsTest extends TestCase {
	private ExecutionContextImpl execCtx;
	
	class DummyStatefulExtension extends AbstractStatefulExtensions<String> {
		public String _set (Object o, String value) {
			return set(o,value);
		}
		public String _get (Object o) {
			return get(o);
		}
		public String _getDefault (Object o) {
			return getDefault(o);
		}
		public Map<Object,String> _getVars () {
			return getVars();
		}
	}
	
	private DummyStatefulExtension ext;
	
	@Override
	public void setUp () {
		execCtx = new ExecutionContextImpl();
		ext = new DummyStatefulExtension();
		ext.setExecutionContext(execCtx);
	}
	
	public void test_getVars () {
		assertNull (execCtx.getGlobalVariables().get(DummyStatefulExtension.class.getName()));
		assertNotNull (ext._getVars());
		// global variable registeed
		assertNotNull (execCtx.getGlobalVariables().get(DummyStatefulExtension.class.getName()));
	}

	public void test_get () {
		assertNull (ext._getVars().get("foo"));
		// call get, expect creation of new variable
		assertNull (ext._get("foo"));
		ext._getVars().put("foo", "bar");
		assertEquals ("bar", ext._getVars().get("foo"));
	}

}
