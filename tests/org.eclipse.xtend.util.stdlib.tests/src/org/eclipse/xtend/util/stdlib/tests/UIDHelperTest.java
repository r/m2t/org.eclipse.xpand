/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.util.stdlib.tests;

import junit.framework.TestCase;

import org.eclipse.xtend.expression.ExecutionContextImpl;
import org.eclipse.xtend.util.stdlib.UIDHelper;

/**
 * @author thoms - Initial contribution and API
 */
public class UIDHelperTest extends TestCase {
	private ExecutionContextImpl execCtx;
	private UIDHelper ext;


	@Override
	public void setUp () {
		execCtx = new ExecutionContextImpl();
		ext = new UIDHelper();
		ext.setExecutionContext(execCtx);
	}

	public void test_uid () {
		String element = "FOO";
		String element2 = "BAR";
		String uid = ext.uid(element);
		// access to same instance must return same uid
		assertEquals (uid, ext.uid(element));
		String uid2 = ext.uid(element2);
		assertNotSame(uid, uid2);
		assertEquals (uid2, ext.uid(element2));
	}


}
