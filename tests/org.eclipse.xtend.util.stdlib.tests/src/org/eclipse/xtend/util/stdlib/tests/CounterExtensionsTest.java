/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.util.stdlib.tests;

import junit.framework.TestCase;

import org.eclipse.xtend.expression.ExecutionContextImpl;
import org.eclipse.xtend.util.stdlib.CounterExtensions;

/**
 * @author thoms - Initial contribution and API
 */
public class CounterExtensionsTest extends TestCase {
	private ExecutionContextImpl execCtx;
	private CounterExtensions counter;
	
	
	@Override
	public void setUp () {
		execCtx = new ExecutionContextImpl();
		counter = new CounterExtensions();
		counter.setExecutionContext(execCtx);
	}

	public void test_counterInc () {
		assertEquals(1, counter.counterInc("foo"));
		assertEquals(2, counter.counterInc("foo"));
		assertEquals(1, counter.counterInc("bar"));
	}
	public void test_counterDec () {
		assertEquals(1, counter.counterInc("foo"));
		assertEquals(2, counter.counterInc("foo"));
		assertEquals(1, counter.counterDec("foo"));
	}
	public void test_counterGet () {
		assertEquals(0, counter.counterGet("foo"));
		assertEquals(1, counter.counterInc("foo"));
		assertEquals(1, counter.counterGet("foo"));
	}
	public void test_counterSet () {
		assertEquals(1, counter.counterInc("foo"));
		assertEquals(3, counter.counterSet("foo",3));
	}
	public void test_counterReset () {
		assertEquals(1, counter.counterInc("foo"));
		assertEquals(2, counter.counterInc("foo"));
		assertEquals(0, counter.counterReset("foo"));
	}
}
